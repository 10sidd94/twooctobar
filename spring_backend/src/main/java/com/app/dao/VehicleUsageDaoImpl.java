package com.app.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.app.pojos.VehicleUsage;

@Repository
public class VehicleUsageDaoImpl implements IVehicleUsageDao {

	@Autowired
	private SessionFactory sf;

	@Override
	public String addVehicleUsage(VehicleUsage vu) {
		return "Vehicle Usage added with ID " + sf.getCurrentSession().save(vu);
	}

	@Override
	public List<VehicleUsage> getUsage() {
		String jpql = "select v from VehicleUsage v";

		return sf.getCurrentSession().createQuery(jpql, VehicleUsage.class).getResultList();

	}

	@Override
	public VehicleUsage getUsageById(int usageId) {
		String jpql = "select u from VehicleUsage u where u.usageId=:usageId";
		return sf.getCurrentSession().createQuery(jpql, VehicleUsage.class).setParameter("usageId", usageId)
				.getSingleResult();
	}

	@Override
	public String updateUsageById(int usageId, int unitCharge) {
		VehicleUsage v = sf.getCurrentSession().find(VehicleUsage.class, usageId);
		v.setUnitCharge(unitCharge);
		return "updated successfully";
	}

	@Override
	public String deleteUsageById(int usageId) {

		Query q = sf.getCurrentSession().createQuery("delete VehicleUsage u where u.usageId =:usageId");

		q.setParameter("usageId", usageId);
		q.executeUpdate();
		return "deleted successfully  ";
	}

}
