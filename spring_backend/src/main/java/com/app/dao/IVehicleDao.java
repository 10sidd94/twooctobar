package com.app.dao;

import java.util.List;

import com.app.pojos.Vehicle;

public interface IVehicleDao {

	public List<Vehicle> listVehicle();

	public  String  addVehicle(Vehicle v);
	
	public String deleteVehicleById(int vId);
	
	public Vehicle getVehicleById(int vId);
	
	public String  updateVehicleById(int vId,int ModelYear);
}
