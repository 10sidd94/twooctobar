package com.app.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.app.pojos.User;
import com.app.pojos.VehicleBooking;

@Repository
public class VehicleBookingDaoImpl implements IVehicleBookingDao{

	@Autowired 
	private SessionFactory sf;
	
	@Override
	public String addVehicleBooking(VehicleBooking v) {
		System.out.println("v: "+v.toString());
		//v.setvId(vId);
		//v.setvId(vId);
		//String jpql = "select u from Vehicle u where u.vId=:vId";
		// Vehicle ve=sf.getCurrentSession().createQuery(jpql, Vehicle.class).setParameter("vId", vId).getSingleResult();
		System.out.println(v);
		
	sf.getCurrentSession().save(v);;
	return "Vehicle registered ";
	}

	@Override
	public List<VehicleBooking> getBooking() {
		String jpql = "select v from VehicleBooking v";
		
		return sf.getCurrentSession().createQuery(jpql, VehicleBooking.class).getResultList();
		
	}
	


	@Override
	public VehicleBooking getBookingById(int bookingId) {
		String jpql = "select u from VehicleBooking u where u.bookingId=:bookingId";
		return sf.getCurrentSession().createQuery(jpql, VehicleBooking.class).setParameter("bookingId", bookingId).getSingleResult();
	}



	@Override
	public String  updateBookingById(int bookingId,String fromPlace) {
	//	String jpql = "update User u set u.password=:pass where u.email=:em";
		// sf.openSession();
		///Transaction tx3 = session3.beginTransaction();
		VehicleBooking v=sf.getCurrentSession().find(VehicleBooking.class, bookingId);
		v.setFromPlace(fromPlace);
		return "updated successfully";
	}



	@Override
	public String deleteBookingById(int bookingId) {
		Query q = sf.getCurrentSession().createQuery("delete VehicleBooking u where u.bookingId =:bookingId");
		
		q.setParameter("bookingId",bookingId);
		q.executeUpdate();
		 return "deleted successfully  ";
	}

}
