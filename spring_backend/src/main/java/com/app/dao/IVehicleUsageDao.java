package com.app.dao;

import java.util.List;

import com.app.pojos.VehicleUsage;

public interface IVehicleUsageDao {

	public String addVehicleUsage(VehicleUsage vu);

	public List<VehicleUsage> getUsage();

	public String deleteUsageById(int userId);

	public VehicleUsage getUsageById(int userId);

	public String updateUsageById(int userId, int unitCharge);

}
