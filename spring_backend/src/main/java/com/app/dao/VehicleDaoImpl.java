package com.app.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.app.pojos.User;
import com.app.pojos.Vehicle;

@Repository
public class VehicleDaoImpl implements IVehicleDao{

	@Autowired // byType
	private SessionFactory sf;

	
	@Override
	public List<Vehicle> listVehicle() {
		String jpql = "select v from Vehicle v";
		return sf.getCurrentSession().createQuery(jpql, Vehicle.class).getResultList();
			
	}


	@Override
	public String addVehicle(Vehicle v) {
		return "Vehicle registered with ID " +sf.getCurrentSession().save(v);
		
	}
	@Override
	public Vehicle getVehicleById(int vId) {
		String jpql = "select u from Vehicle u where u.vId=:vId";
		return sf.getCurrentSession().createQuery(jpql, Vehicle.class).setParameter("vId", vId).getSingleResult();
	}
	
	@Override
	public String  updateVehicleById(int vId,int ModelYear) {
	//	String jpql = "update User u set u.password=:pass where u.email=:em";
		// sf.openSession();
		///Transaction tx3 = session3.beginTransaction();
		Vehicle v=sf.getCurrentSession().find(Vehicle.class, vId);
		v.setModelYear(ModelYear);
//		return sf.getCurrentSession().createQuery(jpql, User.class).setParameter("em", email).setParameter("pass", password).executeUpdate();
		return "updated successfully";
	}
	@Override
	public String deleteVehicleById(int vId) {
//		User u=sf.getCurrentSession().find(User.class, id);
//		sf.getCurrentSession().remove(u);
//		Query query = sf.getCurrentSession().createQuery("delete User u where u.userId =:id");
////		query.setParameter("maxPrice", new Float(1000f));
		Query q = sf.getCurrentSession().createQuery("delete Vehicle v where v.vId =:vId");
		
		q.setParameter("vId",vId);
		q.executeUpdate();
		//int result = query.executeUpdate();
		 
//		String jpql = "delete  User u where u.userId=:id";
//		int count= sf.getCurrentSession().createQuery(jpql, User.class).executeUpdate();
		 return "deleted successfully  ";
	}
	

}
