package com.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.pojos.Vehicle;
import com.app.pojos.VehicleBooking;
import com.app.pojos.VehicleUsage;
import com.app.service.IVehicleUsageService;

@CrossOrigin
@RestController
@RequestMapping("/vehicleUsage") 
public class VehicleUsageController {

	@Autowired
	private IVehicleUsageService service;
	
	
	@PostMapping(value="/addUsage")
	public ResponseEntity<Void> registerVehicle(@RequestBody VehicleUsage vu)
	{
		System.out.println("From Session-------------------");
		System.out.println(vu);
		service.addVehicleUsage(vu);
		
		return new ResponseEntity<Void>(HttpStatus.CREATED);
	}
	@CrossOrigin
	@GetMapping(value="/usList")
	public List<VehicleUsage> getUsage()
	{
		return service.getUsage();
	}
	
	@CrossOrigin
	@PutMapping(value="/{usageId}")
	public String updateVehicleById(@RequestBody VehicleUsage detachedVehicle,@PathVariable int usageId)
	{
		System.out.println("usageId: "+usageId);
		//System.out.println("password : "+detachedUser.getPassword());
		return service.updateUsageById(usageId,detachedVehicle.getUnitCharge());
	}

    @CrossOrigin
	@DeleteMapping(value="/{usageId}")
	public String deleteVehicleById(@PathVariable int usageId)
	{
		System.out.println("usageId: "+usageId);
		return service.deleteUsageById(usageId);
	}
	
    @CrossOrigin
	@GetMapping(value="/{usageId}")
	public VehicleUsage getusageIdById(@PathVariable int usageId)
	{
		System.out.println("usageId : "+usageId);
		return service.getUsageById(usageId);
	}

	
}
