package com.app.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.pojos.User;
import com.app.pojos.UserTypeId;
import com.app.pojos.Vehicle;
import com.app.pojos.VehicleType;
import com.app.service.IVehicleService;

@CrossOrigin
@RestController
@RequestMapping("/vehicle")
public class VehicleController {
	
	@Autowired
	private IVehicleService service;

	@CrossOrigin
	@PostMapping(value="/addVehicle")
	public ResponseEntity<Void> registerVehicle(@RequestBody Vehicle v)
	{
		System.out.println("From Session-------------------");
		System.out.println(v);
		v.setType(VehicleType.CROSSOVER);
		service.addVehicle(v);
		
		return new ResponseEntity<Void>(HttpStatus.CREATED);
	}
	
	@CrossOrigin
	@GetMapping(value="/vList")
	public List<Vehicle> listUsers()
	{
		List<Vehicle> vhList = new ArrayList<Vehicle>();
		List<Vehicle>vList = service.listVehicles();
		for (Vehicle v : vhList) 
		{
			
			vList.add(new Vehicle(v.getvId(),v.getType(), v.getChessisNumber(), v.getModelYear(),v.getActive()));
		}
		return vList;
	}

	
	//update vehicle by id 
     	@CrossOrigin
		@PutMapping(value="/{vId}")
		public String updateVehicleById(@RequestBody Vehicle detachedVehicle,@PathVariable int vId)
		{
			System.out.println("vId: "+vId);
			//System.out.println("password : "+detachedUser.getPassword());
			return service.updateVehicleById(vId,detachedVehicle.getModelYear());
		}
	
	    @CrossOrigin
		@DeleteMapping(value="/{vId}")
		public String deleteVehicleById(@PathVariable int vId)
		{
			System.out.println("vId: "+vId);
			return service.deleteVehicleById(vId);
		}
		
	    @CrossOrigin
		@GetMapping(value="/{vId}")
		public Vehicle getVehicleById(@PathVariable int vId)
		{
			System.out.println("id : "+vId);
			return service.getVehicleById(vId);
		}
}
