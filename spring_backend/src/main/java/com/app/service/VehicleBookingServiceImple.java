package com.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.dao.IVehicleBookingDao;
import com.app.pojos.User;
import com.app.pojos.Vehicle;
import com.app.pojos.VehicleBooking;

@Service
@Transactional
public class VehicleBookingServiceImple implements IVehicleBookingService {

	@Autowired
private IVehicleBookingDao dao;
	
	@Override
	public String addVehicleBooking(VehicleBooking v) {
		return dao.addVehicleBooking(v);
	}

	@Override
	public  List<VehicleBooking> getBooking() {
		return dao.getBooking();
	}

	@Override
	public VehicleBooking getBookingById(int bookingId) {
		return dao.getBookingById(bookingId);
	}

	@Override
	public String updateBookingById(int bookingId,String fromPlace) {
		return dao.updateBookingById(bookingId,fromPlace);
	}

	@Override
	public String deleteBookingById(int bookingId) {
		return dao.deleteBookingById(bookingId);
	}
}
