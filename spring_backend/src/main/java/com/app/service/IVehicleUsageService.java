package com.app.service;

import java.util.List;

import com.app.pojos.Vehicle;
import com.app.pojos.VehicleBooking;
import com.app.pojos.VehicleUsage;

public interface IVehicleUsageService {

	public String addVehicleUsage(VehicleUsage u);
	
	public  List<VehicleUsage> getUsage();
	
	public String deleteUsageById(int usageId) ;
	
	public String updateUsageById(int usageId,int unitCharge);
	
	public VehicleUsage getUsageById(int usageId) ;

}
