package com.app.service;

import java.util.List;

import com.app.pojos.Vehicle;
import com.app.pojos.VehicleBooking;
import com.app.pojos.VehicleUsage;

public interface IVehicleBookingService {

	public String addVehicleBooking(VehicleBooking v);

	public List<VehicleBooking> getBooking();

	public String deleteBookingById(int bookingId);

	public String updateBookingById(int booking, String fromPlace);

	public VehicleBooking getBookingById(int booking);

}
