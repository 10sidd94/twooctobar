package com.app.service;

import java.util.List;

import com.app.pojos.Vehicle;

public interface IVehicleService {

	public List<Vehicle> listVehicles();

	public String addVehicle(Vehicle v);
	
	public String deleteVehicleById(int vId) ;
	
	public String updateVehicleById(int vId,int unitCharge);
	
	public Vehicle getVehicleById(int vId);
}
